package test;

public class Lianbiao {
   private Note head;
   private int size;
   //头添加
   public void add(int index){
       Note newnote = new Note();
       newnote.val = index;
       if(size ==0){
           head = newnote;
       }else{
           newnote.next = head;
           head = newnote;
       }
       size++;
//       if(size!=0){
//           newnote.next = head;
//       }
//       head = newnote;
//       size++;
   }
   //指定位置插入
   public void add1(int index,int val){
       if(index<0||index>size){
           System.out.println("输入错误");
           return ;
       }
       Note newnote = new Note();
       newnote.val = val;
       if(index==0){
           add(val);
       }else{
           Note in = head;
           for(int i=0;i<index-1;i++){
               in = in.next;
           }
           newnote.next = in.next;
           in.next = newnote;
           size++;
       }
   }
   //尾插
    public void add2(int avl){
       add1(size, head.val);

    }
    //查找值为val的位置

   //更改
    public void set(int index,int val){
       if(index<0||index>size){
           System.out.println("输入错误");
           return;
       }
       int i =0;

    }
    //删除操作
    public int remove(int index){
       if(index<0||index>size){
           System.out.println("删除失败");
           return -1;
       }
       if(index==0){
           Note in = head;
         head = in.next;
         in.next = null;
         size--;
         return in.val;
       }else {
           Note in = head;
           for (int i = 0; i < index - 1; i++) {
               in = in.next;
           }
           Note p1 = in.next;
           in.next = p1.next;
           p1.next = null;
           size--;
           return p1.val;
       }
    }

    /***
     * 删除第一个值为val的节点
     * @param val
     */
    public void removeOne(int val){
        Note in = head;
        if(in.val==val){
            head = head.next;
            in.next = null;
            size--;
            return;
        }else{
            for(;in.next!=null;in = in.next){
                if(in.next.val == val){
                    break;
                }
            }
            Note p1=in.next;
            in.next = p1.next;
            p1.next = null;
            size--;

        }
    }

    /**
     * 删除所有值为val的节点
     * @param val
     */
    public void removeAll(int val){
        for(Note in = head;in.next.next!=null;in=in.next){
            if(in.next.val==val){
                Note p1 = in.next;
                in.next = p1.next;
                p1.next = null;
                size--;
            }else{

            }
        }
    }
   //遍历.0
   public String toString(){
       String ret = "";
       Note in = head;
       while (in!=null){
           ret += in.val;
           ret += "->";
           in = in.next;
       }
       ret  = ret + "NULL";
       return ret;
   }

}


class Note{
    int val;
    Note next;
}